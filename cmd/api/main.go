package main

import (
	"log"

	"tweety/configs"
	"tweety/pkg/db"
	"tweety/pkg/handler"
	"tweety/pkg/router"
	"tweety/pkg/store"

	"github.com/spf13/viper"

	_ "github.com/lib/pq"
)

func main() {
	err := configs.LoadConfig()
	if err != nil {
		log.Fatalf(err.Error())
	}

	d := db.New()
	// db.AutoMigrate(d)

	us := store.NewUserService(d)

	r := router.New()
	v1 := r.Group("/api")
	h := handler.NewHandler(us)
	h.Register(v1)

	// Start server
	r.Logger.Fatal(r.Start(viper.GetString("PORT")))
}
