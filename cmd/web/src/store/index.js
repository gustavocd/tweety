import Vue from "vue";
import Vuex from "vuex";
import timeline from "@/store/modules/timeline";
import auth from "@/store/modules/auth";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    timeline,
    auth
  }
});
