import axios from "@/axios";

export default {
  namespaced: true,
  state: {
    tweets: []
  },
  mutations: {
    PUSH_TWEETS(state, data) {
      state.tweets.push(...data);
    }
  },
  getters: {
    tweets(state) {
      return state.tweets;
    }
  },
  actions: {
    async getTweets({ commit }) {
      let response = await axios.get("tweets");
      commit("PUSH_TWEETS", response.data);
    }
  }
};
