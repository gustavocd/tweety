import axios from "@/axios";

export default {
  namespaced: true,
  state: {
    token: null,
    user: null
  },
  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;
    },
    SET_USER(state, user) {
      state.user = user;
    }
  },
  getters: {
    authenticated(state) {
      return state.token !== null && state.user !== null;
    },
    user(state) {
      return state.user;
    }
  },
  actions: {
    async signIn({ dispatch }, credentials) {
      const response = await axios.post("auth/login", credentials);
      return dispatch("attempt", response.data.token);
    },
    async attempt({ state, commit }, token) {
      if (!token || state.token) {
        return;
      }

      commit("SET_TOKEN", token);

      try {
        const response = await axios.get("auth/me");
        commit("SET_USER", response.data.user);
      } catch (error) {
        commit("SET_TOKEN", null);
        commit("SET_USER", null);
      }
    },
    signOut({ commit }) {
      commit("SET_TOKEN", null);
      commit("SET_USER", null);
      localStorage.removeItem("token");
      return Promise.resolve(true);
    }
  }
};
