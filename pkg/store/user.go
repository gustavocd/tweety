package store

import (
	"tweety/pkg/entities"

	"github.com/jinzhu/gorm"
)

// UserService ...
type UserService struct {
	db *gorm.DB
}

// UserStore ...
type UserStore interface {
	GetByID(uint) (*entities.User, error)
	GetByEmail(string) (*entities.User, error)
	GetByUsername(string) (*entities.User, error)
	Create(*entities.User) error
	IsFollower(userID, followerID uint) (bool, error)
	GetFollowingTweets(userID uint) (*[]entities.TweetResponse, error)
	CreateTweet(*entities.Tweet) error
}

// NewUserService ...
func NewUserService(db *gorm.DB) *UserService {
	return &UserService{
		db: db,
	}
}

// GetByID ...
func (us *UserService) GetByID(id uint) (*entities.User, error) {
	var m entities.User
	if err := us.db.First(&m, id).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, nil
		}
		return nil, err
	}
	return &m, nil
}

// GetByEmail ...
func (us *UserService) GetByEmail(e string) (*entities.User, error) {
	var m entities.User
	if err := us.db.Where(&entities.User{Email: e}).First(&m).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, nil
		}
		return nil, err
	}
	return &m, nil
}

// GetByUsername ...
func (us *UserService) GetByUsername(username string) (*entities.User, error) {
	var m entities.User
	if err := us.db.Where(&entities.User{Username: username}).Preload("Followers").First(&m).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, nil
		}
		return nil, err
	}
	return &m, nil
}

// Create ...
func (us *UserService) Create(u *entities.User) (err error) {
	return us.db.Create(u).Error
}

// CreateTweet ...
func (us *UserService) CreateTweet(t *entities.Tweet) (err error) {
	return us.db.Create(t).Error
}

// GetFollowingTweets ...
func (us *UserService) GetFollowingTweets(userID uint) (*[]entities.TweetResponse, error) {
	var tweets []entities.TweetResponse
	ids := []uint{}
	r, err := us.db.Table("following").Select("following_id").Where("following.user_id = ?", userID).Rows()
	if err != nil {
		return nil, err
	}
	for r.Next() {
		var id uint
		r.Scan(&id)
		ids = append(ids, id)
	}

	rows, err := us.db.
		Table("users").
		Select("users.username, users.name, users.id, users.avatar, tweets.body, tweets.id").
		Joins("inner join following on following.following_id = users.id").
		Joins("inner join tweets on tweets.user_id = users.id").
		Where("following.user_id = ? and tweets.user_id IN (?)", userID, ids).
		Order("tweets.created_at DESC").
		Rows()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var tweet entities.TweetResponse
		err := rows.Scan(&tweet.User.Username, &tweet.User.Name, &tweet.User.ID, &tweet.User.Avatar, &tweet.Body, &tweet.ID)
		if err != nil {
			return nil, err
		}
		tweets = append(tweets, tweet)
	}

	return &tweets, nil
}

// IsFollower ...
func (us *UserService) IsFollower(userID, followerID uint) (bool, error) {
	var f entities.User
	if err := us.db.Where("following_id = ? AND user_id = ?", followerID, userID).Find(&f).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}
