package entities

import (
	"github.com/jinzhu/gorm"
)

// Tweet ...
type Tweet struct {
	gorm.Model
	Body   string `json:"body"`
	User   User
	UserID uint `gorm:"foreignkey:UserID"`
}

// TweetResponse ...
type TweetResponse struct {
	ID   uint   `json:"id"`
	Body string `json:"body"`
	User struct {
		ID       uint    `json:"id"`
		Name     string  `json:"name"`
		Username string  `json:"username"`
		Avatar   *string `json:"avatar"`
	} `json:"user"`
}
