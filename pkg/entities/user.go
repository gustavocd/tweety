package entities

import (
	"errors"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// User ...
type User struct {
	gorm.Model
	Username  string `gorm:"unique_index;not null"`
	Name      string `gorm:"not null"`
	Email     string `gorm:"unique_index;not null"`
	Password  string `gorm:"not null"`
	Bio       *string
	Avatar    *string
	Tweets    []Tweet `gorm:"foreignkey:UserID"`
	Followers []*User `gorm:"many2many:followers;association_jointable_foreignkey:follower_id"`
	Following []*User `gorm:"many2many:following;association_jointable_foreignkey:following_id"`
}

// HashPassword ...
func (u *User) HashPassword(plain string) (string, error) {
	if len(plain) == 0 {
		return "", errors.New("password should not be empty")
	}
	h, err := bcrypt.GenerateFromPassword([]byte(plain), bcrypt.DefaultCost)
	return string(h), err
}

// CheckPassword ...
func (u *User) CheckPassword(plain string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(plain))
	return err == nil
}

// FollowedBy ...
func (u *User) FollowedBy(id uint) bool {
	if u.Followers == nil {
		return false
	}
	for _, f := range u.Followers {
		if f.ID == id {
			return true
		}
	}
	return false
}
