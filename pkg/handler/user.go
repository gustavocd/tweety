package handler

import (
	"net/http"
	"tweety/pkg/response"

	"github.com/labstack/echo/v4"
)

// GetProfile ...
func (h *Handler) GetProfile(c echo.Context) error {
	username := c.Param("username")
	u, err := h.userStore.GetByUsername(username)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.NewError(err))
	}

	if u == nil {
		return c.JSON(http.StatusNotFound, response.NotFound())
	}

	return c.JSON(http.StatusOK, newProfileResponse(h.userStore, userIDFromToken(c), u))
}

// CurrentUser ...
func (h *Handler) CurrentUser(c echo.Context) error {
	u, err := h.userStore.GetByID(userIDFromToken(c))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.NewError(err))
	}
	if u == nil {
		return c.JSON(http.StatusNotFound, response.NotFound())
	}
	return c.JSON(http.StatusOK, newUserResponse(u))
}

func userIDFromToken(c echo.Context) uint {
	id, ok := c.Get("user").(uint)
	if !ok {
		return 0
	}
	return id
}
