package handler

import (
	"tweety/pkg/jwttoken"
	"tweety/pkg/router/middleware"

	"github.com/labstack/echo/v4"
)

// Register registers a new router group
func (h *Handler) Register(v1 *echo.Group) {
	jwtMiddleware := middleware.JWT(jwttoken.JWTSecret)
	auth := v1.Group("/auth")
	auth.POST("/login", h.Login)
	auth.POST("/signup", h.SignUp)
	auth.GET("/me", h.CurrentUser, jwtMiddleware)

	// users := v1.Group("/users", jwtMiddleware)

	tweets := v1.Group("/tweets", jwtMiddleware)
	tweets.GET("", h.Tweets)
	tweets.POST("", h.CreateTweet)

	profiles := v1.Group("/profiles", jwtMiddleware)
	profiles.GET("/:username", h.GetProfile)
}
