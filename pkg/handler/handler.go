package handler

import (
	"tweety/pkg/store"
)

// Handler has all stores
type Handler struct {
	userStore store.UserStore
}

// NewHandler creates a new Handler struct
func NewHandler(us store.UserStore) *Handler {
	return &Handler{
		userStore: us,
	}
}
