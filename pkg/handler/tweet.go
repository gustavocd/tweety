package handler

import (
	"net/http"
	"tweety/pkg/entities"
	"tweety/pkg/response"

	"github.com/labstack/echo/v4"
)

// Tweets ...
func (h *Handler) Tweets(c echo.Context) error {
	tweets, err := h.userStore.GetFollowingTweets(userIDFromToken(c))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.NewError(err))
	}
	return c.JSON(http.StatusOK, tweets)
}

// CreateTweet ...
func (h *Handler) CreateTweet(c echo.Context) error {
	var t entities.Tweet
	req := &tweetNewRequest{}
	err := req.bind(c, &t, userIDFromToken(c))
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, response.NewError(err))
	}

	err = h.userStore.CreateTweet(&t)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, response.NewError(err))
	}

	return c.JSON(http.StatusCreated, newTweetResponse(&t))
}
