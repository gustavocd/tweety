package handler

import (
	"net/http"
	"tweety/pkg/entities"
	"tweety/pkg/response"

	"github.com/labstack/echo/v4"
)

// Login authenticates a user into the app
func (h *Handler) Login(c echo.Context) error {
	req := &userLoginRequest{}
	err := req.bind(c)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, response.NewError(err))
	}

	u, err := h.userStore.GetByEmail(req.User.Email)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, response.NewError(err))
	}

	if u == nil {
		return c.JSON(http.StatusForbidden, response.AccessForbidden())
	}

	if !u.CheckPassword(req.User.Password) {
		return c.JSON(http.StatusForbidden, response.AccessForbidden())
	}

	return c.JSON(http.StatusOK, newLoginResponse(u))
}

// SignUp register a new user into the app
func (h *Handler) SignUp(c echo.Context) error {
	var u entities.User
	req := &userRegisterRequest{}
	err := req.bind(c, &u)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, response.NewError(err))
	}

	err = h.userStore.Create(&u)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, response.NewError(err))
	}

	return c.JSON(http.StatusCreated, newUserResponse(&u))
}
