package handler

import (
	"tweety/pkg/entities"
	"tweety/pkg/jwttoken"
	"tweety/pkg/store"
)

type userResponse struct {
	User struct {
		Username string  `json:"username"`
		Name     string  `json:"name"`
		Email    string  `json:"email"`
		Bio      *string `json:"bio"`
		Avatar   *string `json:"avatar"`
	} `json:"user"`
}

func newUserResponse(u *entities.User) *userResponse {
	r := new(userResponse)
	r.User.Username = u.Username
	r.User.Email = u.Email
	r.User.Name = u.Name
	r.User.Bio = u.Bio
	r.User.Avatar = u.Avatar
	return r
}

type loginResponse struct {
	Token string `json:"token"`
}

func newLoginResponse(u *entities.User) *loginResponse {
	r := new(loginResponse)
	r.Token = jwttoken.GenerateJWT(u.ID)
	return r
}

type profileResponse struct {
	Profile struct {
		Username  string  `json:"username"`
		Bio       *string `json:"bio"`
		Avatar    *string `json:"avatar"`
		Following bool    `json:"following"`
	} `json:"profile"`
}

func newProfileResponse(us store.UserStore, userID uint, u *entities.User) *profileResponse {
	r := new(profileResponse)
	r.Profile.Username = u.Username
	r.Profile.Bio = u.Bio
	r.Profile.Avatar = u.Avatar
	r.Profile.Following, _ = us.IsFollower(u.ID, userID)
	return r
}

type tweetResponse struct {
	Tweet struct {
		ID   uint   `json:"id"`
		Body string `json:"body"`
	} `json:"tweet"`
}

func newTweetResponse(t *entities.Tweet) *tweetResponse {
	r := new(tweetResponse)
	r.Tweet.ID = t.ID
	r.Tweet.Body = t.Body
	return r
}
