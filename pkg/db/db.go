package db

import (
	"fmt"
	"log"
	"tweety/pkg/entities"

	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
)

// New creates a new connection to Postgresql using gorm
func New() *gorm.DB {
	psqlInfo := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable",
		viper.GetString("DB_USER"),
		viper.GetString("DB_PASSWORD"),
		viper.GetString("DB_HOST"),
		viper.GetString("DB_NAME"),
	)

	db, err := gorm.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatalf(err.Error())
	}
	db.DB().SetMaxIdleConns(3)
	db.LogMode(true)

	return db
}

// AutoMigrate runs db migrations
func AutoMigrate(db *gorm.DB) {
	db.AutoMigrate(
		&entities.User{},
		&entities.Tweet{},
	)
}
