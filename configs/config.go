package configs

import (
	"errors"

	"github.com/spf13/viper"
)

var (
	// ErrReadInConfig ...
	ErrReadInConfig = errors.New("config: could not read the config file")
)

// LoadConfig loads env variables
func LoadConfig() error {
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		return ErrReadInConfig
	}
	return nil
}
