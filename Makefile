help: # Show help about available commands (https://gist.github.com/prwhite/8168133#gistcomment-1313022)
	@IFS=$$'\n' ; \
	help_lines=(`fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//'`); \
	for help_line in $${help_lines[@]}; do \
			IFS=$$'#' ; \
			help_split=($$help_line) ; \
			help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
			help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
			printf "%-15s %s\n" $$help_command $$help_info ; \
	done


start-local: ## Run the app in local mod you must install Go and PostgreSQL in your machine
	modd

build: ## Run this command after change Dockerfile or docker-compose.yml
	docker-compose build

start-docker: ## Run the app in docker mod no need of installing PostgreSQL
	docker-compose up

clean-docker: ## Clean docker stuff
	docker-compose down
